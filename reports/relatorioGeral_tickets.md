
# Resultados Gerais de Testes

## Resumo

- **Nome do Projeto:** Cinema API
- **Data do Teste:** 21/08/2023 até 01/09/2023
- **Responsável pelo Teste:** Anniely Mariah Soares de Medeiros
- **Versão da API Testada:** 0.0.1
- **Ambiente de Teste:** Local

## Resultados testes funcionais

### Quantidade de testes efetuados

![Alt text](https://media.discordapp.net/attachments/1147666537663184927/1148382323117670531/image.png?width=1407&height=151)

### Defeitos encontrados

- __Todas as rotas:__ Falha nos endpoints que utilizem {id}
- __Criar um ticket:__ Criação de dois tickets iguais

- __Criar um ticket:__ Erro na mensagem de retorno da requisição POST - "Valor do assento deve ser menor ou igual a 100"

### Requisitos atendidos

![Alt text](https://media.discordapp.net/attachments/1147666537663184927/1148381771310825583/image.png?width=1440&height=131)

## Conclusão

Se analizada apenas o endpoint POST, a API encontra com um bom desenvolvimento, apenas tendo dois erros. Entretanto, todas as requisições que estão relacionadas a busca de ID estão quebradas, __impossibilitando__ a aplicação ser enviada pra produção.

## Resultados testes de Performance

### Quantidade de testes efetuados

![Alt text](https://media.discordapp.net/attachments/1147666537663184927/1148382634884477018/image.png?width=951&height=61)

### Testes não passados

- __FU01:__ Aumento expontâneo de volume (Spike Test)

### Requisitos atendidos

![Alt text](https://media.discordapp.net/attachments/1147666537663184927/1148382765453160488/image.png?width=1440&height=52)

## Conclusão

Em relação aos testes funcionais, a aplicação apresenta um bom desempenho.
