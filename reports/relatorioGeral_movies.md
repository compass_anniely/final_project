# Resultados Gerais de Testes

## Resumo

- **Nome do Projeto:** Cinema API
- **Data do Teste:** 21/08/2023 até 01/09/2023
- **Responsável pelo Teste:** Anniely Mariah Soares de Medeiros
- **Versão da API Testada:** 0.0.1
- **Ambiente de Teste:** Local

## Resultados testes funcionais

### Quantidade de testes efetuados

![Alt text](https://media.discordapp.net/attachments/1147666537663184927/1148381620332662864/image.png?width=1407&height=151)

### Defeitos encontrados

- __Todas as rotas:__ Ausência de token administrador/Criação, listagem,  edição e exclusão de filmes por qualquer usuário
- __Criar um filme:__ Ausência de response body na requisição POST
- __Criar um filme:__ Falta de verificação da unicidade dos títulos
- __Criar um filme:__ Showtimes não respeita o bloqueamento por falta de dados
- __Alterar um filme:__ Efetuação de alterações em campos vazios
- __Alterar um filme:__ Erro na documentação, PUT /movies descrito com status 201 ao invés de 200
- __Alterar um filme:__ PUT com body vazio retornando-o como um GET
- __Deletar um filme:__ Erro no retorno DELETE /movies retornando 200 ao invés de 204
- __Deletar um filme:__ Erro na documentação, DELETE /movies descrito com status 201 ao invés de 204

### Requisitos atendidos

![Alt text](https://media.discordapp.net/attachments/1147666537663184927/1148382492206841976/image.png?width=1440&height=350)

## Conclusão

Dentre os requisitos apresentados, 10 constaram sucesso, 3 falhas e 3 requisitos bloqueados. 

Sendo os seguintes requisitos:
- O sistema valida os campos obrigatórios e a unicidade do título.
- O sistema retorna uma resposta de sucesso com o status 201 Created, incluindo o ID do filme.
- O sistema retorna uma resposta de sucesso com o status 204 No Content.

Além disso, foram elencados 9 erros dentre as requisições efetuadas. 

## Resultados testes de Performance

### Quantidade de testes efetuados

![Alt text](https://media.discordapp.net/attachments/1147666537663184927/1148382045442146354/image.png?width=951&height=181)

### Testes não passados

- __FU01:__ Aumento expontâneo de volume (Spike Test)
- __FU01:__ Verificação de limite de requisições por segundo 
- __FU03:__ Aumento expontâneo de volume (Spike Test)
- __FU03:__ Estresse da aplicação (Stress Test)
- __FU03-3:__ Usuários simultâneos (Load Test)
- __FU04:__ Aumento expontâneo de volume (Spike Test)

### Requisitos atendidos

![Alt text](https://media.discordapp.net/attachments/1147666537663184927/1148382153059614811/image.png?width=1440&height=222)

## Conclusão

Priorização da rota POST - Criar filmes, GET {__id} - Listar um filme e PUT - Atualizar um filme.     
