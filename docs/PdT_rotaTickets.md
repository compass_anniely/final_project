# Planejamento de Teste: Validando reserva de ingressos

### Sumário

[__1. INTRODUÇÃO__](#introdução)

[1.1 Visão geral;](#11-visão-geral)

[1.2 Objetivo;](#12-objetivo)

[1.3 Cobertura;](#13-cobertura)

[1.3.1 Mapeamento da API;](#131-mapeamento-da-api)

[1.4 Responsáveis;](#14-responsáveis)

[1.5 Referências/Documentações do projeto.](#15-referênciasdocumentações-do-projeto)

[__2. ABORDAGEM DE TESTES__](#abordagem-de-testes)

[2.1 Critérios e requisitos;](#21-critérios-e-requisitos)

[2.2 Ferramentas.](#22-ferramentas)

[__3. AMBIENTE__](#ambiente)

[3.1 Definições do ambiente de teste.](#31-definições-do-ambiente-de-teste)

[__4. TESTES FUNCIONAIS__](#testes-funcionais)

[4.1 Funcionalidades/Módulos a serem testados;](#41-funcionalidadesmódulos-a-serem-testados)

[__5. TESTES DE PERFORMANCE__](#testes-de-performance)

[5.1 Testes selecionados para execução;](#51-testes-selecionados-para-execução)

[5.2 Métricas delimitadas;](#52-métricas-delimitadas)

[5.2.1 Métricas por verbo;](#521-métricas-por-verbo)

[5.3 Funcionalidades/Módulos a serem testados;](#53-funcionalidadesmódulos-a-serem-testados)

[5.3.1 Tabela de testes por funcionalidade](#531-tabela-de-testes-por-funcionalidade)

[6. __RISCOS__](#riscos)

## Introdução

### 1.1 Visão geral

O principal propósito desta documentação é assegurar a clareza e a organização essenciais para a execução desses testes necessários para validação das funcionalidades requisitadas conforme o Product Owner (PO) informou em sua User Story.
A seguinte documentação irá incluir o processo de realização de testes, que será efetuado com base em três estágios principais:

- Apresentação dos critérios impostos pelo solicitante da implementação;
- Mapeamento da API e análise exploratória sobre suas rotas.
- Definição das estratégias de teste necessárias, métodos e processos a serem utilizados;
- Condições/Casos de Teste onde documentam os testes a serem aplicados, juntamente com seus possíveis riscos.

### 1.2 Objetivo

Realizar testes para validação das ações efetuadas pelo cenário solicitado onde: O usuário deseja reservar ingressos para assistir a um filme em um cinema. Os testes serão com o intuito de avaliar o retorno de suas requisições e verificar a taxa de sucesso das solicitações, garantindo a qualidade de sua navegação.

### 1.3 Cobertura

O plano de teste irá cobrir o CRUD da rota /tickets com base nos repectivos requisitos funcionais e não funcionais (performance) elecados pela User Story: Reservando Ingressos na API.

### 1.3.1 Mapeamento da API

![Mapeamento](https://media.discordapp.net/attachments/1027385935333171220/1143970908877373480/Cinema_API.png?width=1442&height=648)

### 1.4 Responsáveis

Título | Responsável 
-- | -- 
Gerenciamento de projeto | Anniely M. S. de Medeiros  
Testadora  | Anniely M. S. de Medeiros 

### 1.5 Referências/Documentações do projeto

- [Documento de referência para planejamento de teste](https://pt.slideshare.net/LeandroRodrigues221/modelo-de-especificao-de-caso-de-uso)

- [User Story: Reservando Ingressos na API](https://github.com/juniorschmitz/nestjs-cinema/blob/main/UserStories/Reservando%20Ingressos%20na%20API.md)

## Abordagem de Testes

### 2.1 Critérios e requisitos 

Requisitos Funcionais  | Requisito Não Funcionais 
-- | --
__RF01__ - O usuário envia uma solicitação POST para o endpoint /tickets com os seguintes detalhes do ingresso: ID do Filme (movieId), ID do Usuário (userId), Número do Assento (seatNumber), Preço do Ingresso (price) e Data de Apresentação (showtime)| __RP01__ - A API deve ser capaz de processar pelo menos 50 solicitações de reserva de ingressos por segundo.
__RF02__ - O sistema valida se todos os campos obrigatórios estão preenchidos corretamente. | __RP02__ - O tempo médio de resposta para a reserva de um ingresso não deve exceder 300 milissegundos.
__RF03__ - O sistema verifica se o número do assento está dentro do intervalo de 0 a 99.
__RF04__ - O sistema verifica se o preço do ingresso está dentro do intervalo de 0 a 60.
__RF05__ - Se todas as validações passarem, o sistema cria uma reserva de ingresso com os detalhes fornecidos.
__RF06__ - O sistema atribui um ID único à reserva de ingresso.
__RF07__ - O sistema retorna uma resposta de sucesso com o status 201 Created, incluindo o ID da reserva de ingresso.

### 2.2 Ferramentas 

As seguintes ferramentas serão empregadas neste projeto de testes:

Ferramenta |  Versão 
---------- | ------- 
Visual Studio Code | 1.80.2
Postman | 10.17.3
XMind      |22.11 (3771)

## Ambiente

### 3.1 Definições do ambiente de teste
Para melhor desempenho, será utilizado os seguintes requisitos:

- As máquinas terão como sistema operacional Microsoft Windows 11 - Versão 22H2;
- Será disponibilizado ambiente virtual com a utilização da tecnologia cloud EC2 da Amazon Web Services.

## Testes Funcionais
Para a realização dos testes funcionais, serão elencados testes de acordo com cada endpoint do CRUD de /tickets, respeitando sua documentação swagger e critérios impostos nos requisitos previamente citados.

### 4.1 Funcionalidades/Módulos a serem testados

Funcionalidade | ID - Caso de uso | Detalhamento | Requisitos que motivaram o teste
-- | -- | -- | --
Criar um novo ticket | 01 - Inserir dados válidos nos campos: MovieID, userID, seatNumber, Price e Showtime | Verificar o retorno da requisição com status 201 incluindo ID da reserva do ingresso | RF01 - RF07
 | | 02 - Inserir dados já utilizados, criar ticket já existente | Verificar e validar a não criação do ticket | 
 | | 03 - Solicitar a geração de um Ticket com  um dado ausentes | Verificar e validar a não criação do ticket  | RF02
 | | 04 - Solicitar a geração de um Ticket sem body | Verificar e validar a não criação do ticket | RF02
 | | 05 - Solicitar a geração de um Ticket com assentos de numeração menor que 0 e maior que 99 | Verificar e validar a não criação do ticket  | RF03
 | | 06 - Solicitar a geração de um Ticket com valor/preço menor que 0 e maior que 60 | Verificar e validar a não criação do ticket | RF04

Funcionalidade | ID - Caso de uso | Detalhamento | Requisitos que motivaram o teste
-- | -- | -- | --
Listar tickets | 01 - Solicitar todos as reservas geradas com os detalhes dos campos previamente fornecidos | Validar a integridade da base de dados gerada e verificar a existência de IDs únicos para cada reserva de ingresso | RF05 - RF06 
| | 02 - Solicitar a busca de um único ticket com base em um ID registrado | Verificar o retorno com os campos cadastrados de acordo com o solicitado | RF05
| | 03 - Solicitar a busca de um único ticket com base em um ID não registrado | Verificar o não retorno/ausência do item buscado no banco de dados |

Funcionalidade | ID - Caso de uso | Detalhamento | Requisitos que motivaram o teste
-- | -- | -- | --
Atualizar um ticket | 01 - Alterar dados de um ticket previamente registrados | Verificar a atualização de um ticket previamente cadastrado | 
| | 02 - Alterar dados de um ticket não registrado | Validar a não alteração de um ticket não registrado | |
| | 03 - Alterar dados de um ticket registrado com dados "vazios" | Validar a não alteração de um ticket "nulo" | |

Funcionalidade | ID - Caso de uso | Detalhamento | Requisitos que motivaram o teste
-- | -- | -- | --
Deletar um ticket | 01 - Solicitar a exclusão de um ticket previamente registrado | Verificar a atualização no banco de dados removendo o registro excluído | |
| | 02 - Solicitar a exclusão de um ticket não registrado | Sinalizando a ausência do ticket buscado e não deletando nada no banco de dados | |

## Testes de Performance

### 5.1 Testes selecionados para execução

__Validação de Rotas (Smoke Test):__ Primeira verificação para saber se as funcionalidades estão de acordo com o proposto.

__Usuários simultâneos (Load Test):__ Verificação do comportamento da aplicação ao submetida a várias requisições.

__Concorrência (Concurrence Test):__ Validação da navegação de usuários em dois ou mais volumes distintos submetendo requisições.

__Aumento expontâneo de volume (Spike Test):__ Muitos usuários acessando em um curto período de tempo.

__Estresse da aplicação (Stress Test):__ Utilização da capacidade máxima indicada na aplicação por um determinado período de tempo.

__Longa duração (Soak Test):__ Conjunto de usuários executando ações durante um período de tempo.

__Validação de Rotas (Smoke Test):__ Primeira verificação para saber se as funcionalidades estão de acordo com o proposto.

__Usuários simultâneos (Load Test):__ Verificação do comportamento da aplicação ao submetida a várias requisições.

__Concorrência (Concurrence Test):__ Validação da navegação de usuários em dois ou mais volumes distintos submetendo requisições.

__Aumento expontâneo de volume (Spike Test):__ Muitos usuários acessando em um curto período de tempo.

__Estresse da aplicação (Stress Test):__ Utilização da capacidade máxima indicada na aplicação por um determinado período de tempo.

__Longa duração (Soak Test):__ Conjunto de usuários executando ações durante um período de tempo.

### 5.2 Métricas delimitadas

__Quantidade média de usuários:__ 50-100

__Quantidade máxima de usuários:__ 200

__Margem de aumento:__ 50% a 75%

__Duração do testes de resistência:__ 5 minutos

__Tempo de realização da bateria de testes:__ Aproximadamente 1 hora

### 5.2.1 Métricas por verbo

| | POST - Criar um ticket 
| - | - |
| Quantidade de requisições suportadas por seg e min | 50/3000 
Tempo de resposta indicado | 150ms 
Tempo médio de resposta | 300ms 
Tempo máximo de resposta | Não definido 

### 5.3 Funcionalidades/Módulos a serem testados

| ID | Funcionalidade |
|----|----------------|
|FU01| Criar um ticket |

### 5.3.1 Tabela de testes por funcionalidade

Funcionalidades a serem testadas | Tipo de teste | ID | Duração | Usuários virtuais | Objetivo do testes
| - | - | - | - | - | - |
FU01 | Validação de Rota (Smoke Test) | - | 1 seg | 1 | Verificar a integridade da requisição
FU01 | Verificação de limite de requisições por segundo | - | 30 seg | 1 | Verificar se a requisição está atendendo a quantidade indicada de 50 requisições por segundo.
FU01 | Usuários simultâneos (Load Test) | 1 | 40 seg | 25 | Verificação do comportamento da aplicação ao submetida a 25 usuários simultâneos
FU01 | Usuários simultâneos (Load Test) | 2 | 90 seg | 50 | Verificação do comportamento da aplicação ao submetida a 50 usuários simultâneos
FU01 | Usuários simultâneos (Load Test) | 3 | 90 seg | 100 | Verificação do comportamento da aplicação ao submetida a 100  usuários simultâneos
FU01 | Concorrência (Concurrence Test) | - | 60 seg | 50 | Verificação do comportamento da aplicação quando submetida a duas massas simultâneas realizando requisições em tempos e quantidades distintas.
FU01 | Longa duração (Soak Test) | - | 330 seg | 100 | Verificar o comportamento e integridade da API quando submetida a um volume padrão de requisições até um limite de tempo.
FU01 | Estresse da aplicação (Stress Test) | - | 45 seg seg | 200 | Simular um aumento de requisições acima do comumento suportado pela API
FU01 | Aumento expontâneo de volume (Spike Test) | - | 5 seg | 500 | Verificar o comportamento da API quando submetida a um grande volume requisições em um pequeno espaço de tempo  

## Riscos

__Retorno de Status Inadequado:__ A API pode retornar status codes incorretos, como 202 em vez de 401.

__Comprometimento do Banco de Dados:__ A possibilidade de ocorrer uma degradação no banco de dados, impossibilitando a realização dos testes.

__Comprometimento nas Requisições:__ Quebra de script ou defeitos/erros graves referentes a construção da API, impossibilitando a geração de mais testes. 

__Desempenho sob Carga Elevada:__ A API pode não suportar cargas elevadas, levando a tempos de resposta lentos ou indisponibilidade.

__Inconsistência no Tempo de Resposta:__ Problemas como latência podendo causar tempos de resposta inconsistentes.

__Falta de Evidência Adequada:__ Pode haver dificuldades na documentação adequada dos resultados dos testes.

Dentre outros.
