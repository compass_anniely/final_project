# Planejamento de Teste: Gerenciamento de Filmes na API

### Sumário

[__1. INTRODUÇÃO__](#introdução)

[1.1 Visão geral;](#11-visão-geral)

[1.2 Objetivo;](#12-objetivo)

[1.3 Cobertura;](#13-cobertura)

[1.3.1 Mapeamento da API;](#131-mapeamento-da-api)

[1.4 Responsáveis;](#14-responsáveis)

[1.5 Referências/Documentações do projeto.](#15-referênciasdocumentações-do-projeto)

[__2. ABORDAGEM DE TESTES__](#abordagem-de-testes)

[2.1 Critérios e requisitos;](#21-critérios-e-requisitos)

[2.1.1 Requisitos funcionais;](#211-requisitos-funcionais)

[2.1.2 Requisitos não funcionais;](#212-requisitos-não-funcionais)

[2.2 Ferramentas.](#22-ferramentas)

[__3. AMBIENTE__](#ambiente)

[3.1 Definições do ambiente de teste.](#31-definições-do-ambiente-de-teste)

[__4. TESTES FUNCIONAIS__](#testes-funcionais)

[4.1 Funcionalidades/Módulos a serem testados;](#41-funcionalidadesmódulos-a-serem-testados)

[__5. TESTES DE PERFORMANCE__](#testes-de-performance)

[5.1 Testes selecionados para execução;](#51-testes-selecionados-para-execução)

[5.2 Métricas delimitadas;](#52-métricas)

[5.2.1 Métricas gerais;](#521-métricas-gerais)

[5.2.2 Métricas por verbo;](#522-métricas-por-verbo)

[5.3 Funcionalidades/Módulos a serem testados;](#53-funcionalidadesmódulos-a-serem-testados) 

[5.3.1 Tabela de testes por funcionalidade](#532-tabela-de-testes-por-funcionalidade)

[6. __RISCOS__](#riscos)

## Introdução

### 1.1 Visão geral

O principal propósito desta documentação é assegurar a clareza e a organização essenciais para a execução desses testes necessários para validação das funcionalidades requisitadas conforme o Product Owner (PO) informou em sua User Story.
A seguinte documentação irá incluir o processo de realização de testes, que será efetuado com base em três estágios principais:

- Apresentação dos critérios impostos pelo solicitante da implementação;
- Mapeamento da API e análise exploratória sobre suas rotas.
- Definição das estratégias de teste necessárias, métodos e processos a serem utilizados;
- Condições/Casos de Teste onde documentam os testes a serem aplicados, juntamente com seus possíveis riscos.

### 1.2 Objetivo

Realizar testes para validação das ações efetuadas para a visualização do catálogo de filmes para usuários comuns e criação, alteração e exclusão para usuários administradores. Os testes serão com o intuito de avaliar o retorno de suas requisições e verificar a taxa de sucesso das solicitações, garantindo a qualidade de sua navegação.

### 1.3 Cobertura

O plano de teste irá cobrir o CRUD da rota /movies com base nos repectivos requisitos funcionais e não funcionais (performance) elecados pela User Story: Gerenciamento de Filmes na API.

### 1.3.1 Mapeamento da API

![Mapeamento](https://media.discordapp.net/attachments/1027385935333171220/1143970908877373480/Cinema_API.png?width=1442&height=648)

### 1.4 Responsáveis

Título | Responsável 
-- | -- 
Gerenciamento de projeto | Anniely M. S. de Medeiros  
Testadora | Anniely M. S. de Medeiros 

### 1.5 Referências/Documentações do projeto

- [Documento de referência para planejamento de teste](https://pt.slideshare.net/LeandroRodrigues221/modelo-de-especificao-de-caso-de-uso)

- [User Story: Gerenciamento de Filmes na API](https://github.com/juniorschmitz/nestjs-cinema/blob/main/UserStories/Gerenciamento%20de%20Filmes%20na%20API.md)

## Abordagem de Testes

### 2.1 Critérios e requisitos 

### 2.1.1 Requisitos funcionais

Ação efetuada | ID | Passos requisitados
-- | -- | --
Criando um filme | RF01-1 | O usuário administrador da API envia uma solicitação POST para o endpoint /movies com os detalhes do filme.
| | RF01-2 | O sistema valida os campos obrigatórios e a unicidade do título.
| | RF01-3 | Se as validações passarem, o sistema cria o filme e atribui um ID único.
| | RF01-4 | O sistema retorna uma resposta de sucesso com o status 201 Created, incluindo o ID do filme.

Ação efetuada | ID | Passos requisitados
-- | -- | --
Obtendo a lista de filmes | RF02-1 | O usuário envia uma solicitação GET para o endpoint /movies.
| | RF02-2 | O sistema retorna uma lista de todos os filmes cadastrados com detalhes.

Ação efetuada | ID | Passos requisitados
-- | -- | --
| Obtendo Detalhes de um Filme por ID | RF03-1 | O usuário envia uma solicitação GET para o endpoint /movies/{id}, onde {id} é o ID do filme desejado.
| | RF03-2 | O sistema verifica a existência do filme e retorna seus detalhes.
| | RF03-3 | Se o filme não existir, o sistema retorna uma resposta de erro com o status 404 Not Found.

Ação efetuada | ID | Passos requisitados
-- | -- | --
Atualizando os Detalhes de um Filme por ID | RF04-1 | O usuário administrador da API envia uma solicitação PUT para o endpoint /movies/{id}, onde {id} é o ID do filme a ser atualizado.
| | RF04-2 | O sistema verifica a existência do filme, permite a atualização de campos específicos e valida os dados.
| | RF04-3 | Se todas as validações passarem, o sistema atualiza os detalhes do filme.
| | RF04-4 | O sistema retorna uma resposta de sucesso com o status 200 OK e os detalhes atualizados do filme.

Ação efetuada | ID | Requisitos
-- | -- | --
| Excluindo um Filme por ID | RF05-1 | O usuário administrador da API envia uma solicitação DELETE para o endpoint /movies/{id}, onde {id} é o ID do filme a ser excluído.
| | RF05-2 | O sistema verifica a existência do filme e o remove permanentemente do banco de dados.
| | RF05-3 | O sistema retorna uma resposta de sucesso com o status 204 No Content.

### 2.1.2 Requisitos não funcionais

ID | Requisitos
-- | --
RP01 | A API deve ser capaz de processar pelo menos 100 solicitações de criação de filmes por segundo.
RP02 | O tempo médio de resposta para a criação de um novo filme não deve exceder 200 milissegundos.
RP03 | A API deve ser capaz de responder a solicitações GET de listagem de filmes em menos de 100 milissegundos.
RP04 | A lista de filmes deve ser paginada, com no máximo 20 filmes por página.
RP05 | A API deve ser capaz de responder a solicitações GET de detalhes de um filme em menos de 50 milissegundos.
RP06 | A API deve ser capaz de processar pelo menos 50 solicitações de atualização de filmes por segundo.
RP07 | O tempo médio de resposta para a atualização dos detalhes de um filme não deve exceder 300 milissegundos.
RP08 | A API deve ser capaz de processar pelo menos 30 solicitações de exclusão de filmes por segundo.
RP09 | O tempo médio de resposta para a exclusão de um filme não deve exceder 400 milissegundos.

### 2.2 Ferramentas 

As seguintes ferramentas serão empregadas neste projeto de testes:

Ferramenta |  Versão 
---------- | ------- 
Visual Studio Code | 1.80.2
Postman | 10.17.3
XMind | 22.11 (3771)

## Ambiente

### 3.1 Definições do ambiente de teste
Para melhor desempenho, será utilizado os seguintes requisitos:

- As máquinas terão como sistema operacional Microsoft Windows 11 - Versão 22H2;
- Será disponibilizado ambiente virtual com a utilização da tecnologia cloud EC2 da Amazon Web Services.

## Testes Funcionais
Para a realização dos testes funcionais, serão elencados testes de acordo com cada endpoint do CRUD de /movies, respeitando sua documentação swagger e critérios impostos nos requisitos previamente citados.

### 4.1 Funcionalidades/Módulos a serem testados

Funcionalidade | ID - Caso de uso | Detalhamento | Requisitos que motivaram o teste
-- | -- | -- | --
Criar um novo filme | 01 - Usuário administrador solicitando o cadastro de um novo filme com todos os dados | Verificar o retorno da requisição com status 201 incluindo ID do filme registrado | RF01-1, RF01-2, RF01-3 e RF01-4
| | 02 - Usuário comum solicitando o cadastro de um novo filme com todos os dados | Verificar o retorno da requisição impedindo a criação do filme | RF01-1
| | 03 - Cadastrar filme com todos os campos sendo nome já registrado no banco de dados | Verificar a criação do filme desrespeitando a unicidade do título | RF01-2
| | 04 - Cadastrar filme com dados ausentes | Validar a obrigatóriedade da inclusão de dados nos campos solicitados | RF01-2
| | 05 - Cadastrar filme sem dados | Verificar o retorno da requisição sem nenhum dado enviado | RF01-2

Funcionalidade | ID - Caso de uso | Detalhamento | Requisitos que motivaram o teste
-- | -- | -- | --
Listar filmes | 01 - Usuário administrador solicitando todos os filmes previamente registrados no banco de dados | Validar a integridade da base de dados gerada e verificar a existência de IDs únicos para cada filme cadastrado | RF02-1 e RF02-2
| | 02 - Usuário comum solicitando todos os filmes previamente registrados no banco de dados| Validar a integridade da base de dados gerada e retornada com os detalhes dos filmes | RF02-1 e RF02-2
| | 03 - Solicitar a busca de um único filme com base em um ID registrado | Verificar o retorno com os campos cadastrados de acordo com o solicitado | RF03-1 e RF03-2
| | 04 - Solicitar a busca de um único filme com base em um ID não registrado | Verificar o não retorno/ausência do item buscado no banco de dados | RF03-1 e RF03-3

Funcionalidade | ID - Caso de uso | Detalhamento | Requisitos que motivaram o teste
-- | -- | -- | --
Atualizar um filme | 01 - Usuário administrador alterando dados de um filme previamente registrados | Verificar a atualização de um filme previamente cadastrado | RF04-1, RF04-2, RF04-3 e RF04-4
| | 02 - Usuário comum alterando dados previamente registrados | Verificar a atualização não efetuada de um filme previamente cadastrado | RF04-1 e RF04-2
| | 03 - Usuário administrador alterando dados para "vazios" de um filme previamente registrados | Verificar a não atualização de um filme previamente cadastrado | RF04-1, RF04-2

Funcionalidade | ID - Caso de uso | Detalhamento | Requisitos que motivaram o teste
-- | -- | -- | --
Deletar um filme | 01 - Usuário administrador solicitando a exclusão de um filme previamente registrado | Verificar a atualização no banco de dados removendo o registro excluído | RF05-1, RF05-2, RF05-3 |
| | 02 - Usuário comum solicitando a exclusão de um filme previamente registrado | Verificar a atualização no banco de dados não removendo o registro impedindo-o de ser deletado | RF05-1, RF05-2, RF05-3 |
| | 02 - Solicitar a exclusão de um filme não registrado | Sinalizando a ausência do filme buscado e não deletando nada no banco de dados | |

## Testes de Performance

### 5.1 Testes selecionados para execução

__Validação de Rotas (Smoke Test):__ Primeira verificação para saber se as funcionalidades estão de acordo com o proposto.

__Usuários simultâneos (Load Test):__ Verificação do comportamento da aplicação ao submetida a várias requisições.

__Concorrência (Concurrence Test):__ Validação da navegação de usuários em dois ou mais volumes distintos submetendo requisições.

__Aumento expontâneo de volume (Spike Test):__ Muitos usuários acessando em um curto período de tempo.

__Estresse da aplicação (Stress Test):__ Utilização da capacidade máxima indicada na aplicação por um determinado período de tempo.

__Longa duração (Soak Test):__ Conjunto de usuários executando ações durante um período de tempo.

### 5.2 Métricas 

### 5.2.1 Métricas gerais
__Quantidade média de usuários:__ 50-100

__Quantidade máxima de usuários:__ 200

__Margem de aumento:__ 50% a 75%

__Duração do testes de resistência:__ 5 minutos

__Tempo de realização da bateria de testes:__ Aproximadamente 1 hora

### 5.2.2 Métricas por verbo

| | POST - Criar um filme | GET - Listar filmes | GET - Listar um filme | PUT - Alterar um filme | DEL - Deletar um filme |
| - | - | - | - | - | - |
| Quantidade de requisições suportadas por seg e min | 100/6000 | Não definido | Não definido | 50/3000 | 30/1800
Tempo de resposta indicado | 100ms | 45ms | 25ms | 150ms | 200ms
Tempo médio de resposta | 200ms | Não definido | Não definido | 300ms | 400ms
Tempo máximo de resposta | Não definido | 100ms | 50ms | Não definido | Não definido

### 5.3 Funcionalidades/Módulos a serem testados

| ID | Funcionalidade |
|----|----------------|
|FU01| Criar um filme |
|FU02| Listar filmes|
|FU03| Listar um filme|
|FU04| Alterar um filme|
|FU05| Deletar um filme|



### 5.3.1 Tabela de testes por funcionalidade

Funcionalidades a serem testadas | Tipo de teste | ID | Duração | Usuários virtuais | Objetivo do testes
| - | - | - | - | - | - |
FU01, FU02, FU03, FU04 e FU05 | Validação de Rota (Smoke Test) | - | 1 seg | 1 | Verificar a integridade da requisição
FU05 | Verificação de limite de requisições por segundo | 1 | 30 seg | 1 | Verificar se a requisição Deletar um filme está atendendo a quantidade indicada de 25 requisições por segundo.
FU04 | Verificação de limite de requisições por segundo | 2 | 30 seg | 1 | Verificar se a requisição Alterar um filme está atendendo a quantidade indicada de 50 requisições por segundo.
FU01 | Verificação de limite de requisições por segundo | 3 | 30 seg | 1 | Verificar se a requisição Criar um filme está atendendo a quantidade indicada de 100 requisições por segundo.
FU01, FU02, FU03, FU04 e FU05 | Usuários simultâneos (Load Test) | 1 | 40 seg | 25 | Verificação do comportamento da aplicação ao submetida a 25 usuários simultâneos
FU01, FU02, FU03 e FU04 | Usuários simultâneos (Load Test) | 2 | 90 seg | 50 | Verificação do comportamento da aplicação ao submetida a 50 usuários simultâneos
FU01, FU02, FU03 e FU04 | Usuários simultâneos (Load Test) | 3 | 90 seg | 100 | Verificação do comportamento da aplicação ao submetida a 100  usuários simultâneos
FU01, FU02, FU03 e FU04 | Concorrência (Concurrence Test) | - | 60 seg | 50 | Verificação do comportamento da aplicação quando submetida a duas massas simultâneas realizando requisições em tempos e quantidades distintas.
FU01, FU02, FU03 e FU04 | Longa duração (Soak Test) | - | 330 seg | 100 | Verificar o comportamento e integridade da API quando submetida a um volume padrão de requisições até um limite de tempo.
FU01, FU02, FU03 e FU04 | Estresse da aplicação (Stress Test) | - | 45 seg seg | 200 | Simular um aumento de requisições acima do comumento suportado pela API
FU01, FU02, FU03 e FU04 | Aumento expontâneo de volume (Spike Test) | - | 5 seg | 500 | Verificar o comportamento da API quando submetida a um grande volume requisições em um pequeno espaço de tempo  

## Riscos

__Retorno de Status Inadequado:__ A API pode retornar status codes incorretos, como 202 em vez de 401.

__Comprometimento do Banco de Dados:__ A possibilidade de ocorrer uma degradação no banco de dados, impossibilitando a realização dos testes.

__Comprometimento nas Requisições:__ Quebra de script ou defeitos/erros graves referentes a construção da API, impossibilitando a geração de mais testes. 

__Desempenho sob Carga Elevada:__ A API pode não suportar cargas elevadas, levando a tempos de resposta lentos ou indisponibilidade.

__Inconsistência no Tempo de Resposta:__ Problemas como latência podendo causar tempos de resposta inconsistentes.

__Falta de Evidência Adequada:__ Pode haver dificuldades na documentação adequada dos resultados dos testes.

Dentre outros.
