# Testes na API Cinema
Este repositório foi criado com o propósito de armazenar toda a documentação e os scripts desenvolvidos para a realização de testes na API Cinema. Para manter uma organização adequada, o projeto foi dividido em duas branches principais: main e K6. Abaixo, apresentamos um resumo de todos os componentes e informações relevantes do projeto.

## Sumário
1. [Documentações Elaboradas](#documentações-elaboradas)
2. [Softwares Utilizados](#softwares-utilizados)
3. [Estruturação do Repositório](#estruturação-do-repositório)
4. [Relatório de Resultados](#4-relatório-de-resultados)
5. [Scripts Gerados](#scripts-gerados)
6. [Contribuintes](#contribuintes)
7. [Licença](#licença)

## Documentações Elaboradas
Para o planejamento e a execução dos testes na API Cinema, foram elaboradas as seguintes documentações:

### [1. Planejamento de Teste: Gerenciamento de Filmes na API](/docs/PdT_rotaMovies.md)
Esta documentação detalha o planejamento e os casos de teste relacionados aos requisitos de usuário sobre as funcionalidades disponíveis na rota /movies da API Cinema. Ela oferece uma visão abrangente dos testes funcionais e de performance específicos para essa área.

### [2. Planejamento de Teste: Validando Reserva de Ingressos](/docs/PdT_rotaTickets.md)
Esta documentação abrange os testes planejados e executados nas operações relacionadas à compra de ingressos no sistema. Ela fornece informações sobre como os testes funcionais e de performance foram estruturados para essa funcionalidade.

### [3. Execução de testes no K6](https://gitlab.com/compass_anniely/final_project/-/blob/k6/README.md?ref_type=heads)
Anexamos uma documentação explicando a instalação do Faker e K6, além de instruções detalhadas sobre como manipular os testes e desempenho usando esses scripts.

### [4. Relatório de Resultados](/reports/)
Após a execução dos testes, um relatório completo de resultados será gerado e disponibilizado neste repositório. Este relatório incluirá um resumo dos resultados obtidos e anexados do projeto do JIRA.

- __[Relatório geral /movies](/reports/relatorioGeral_movies.md)__
- __[Relatório geral /tickets](/reports/relatorioGeral_tickets.md)__

## Softwares Utilizados
Neste projeto, foram utilizados os seguintes softwares e ferramentas:

- __Postman__ - Ferramenta de testes funcionais.
- __K6__ - Ferramenta de código aberto para testes de carga e desempenho.

## Estruturação do Repositório
O repositório está organizado da seguinte forma:

__[/docs](/docs/)__ - Pasta contendo a documentação principal dos testes funcionais e de performance.

__[/reports](/reports/)__ - Local onde os relatórios de resultados serão armazenados após a execução dos testes. Dentre eles, temos:
- __[/functional_test_results](/reports/functional_test_results/)__
- __[/performance_test_results](/reports/performance_test_results/)__

__[/K6](https://gitlab.com/compass_anniely/final_project/-/tree/k6?ref_type=heads)__ - Branch dedicada exclusivamente aos scripts de testes de carga e desempenho usando o K6.

## Relatório de Resultados

Após a execução dos testes, os relatórios de resultados serão disponibilizados na pasta [__/reports__](/reports/). Eles incluirão um resumo das informações sobre o desempenho da API, relatando os resultados dos testes funcionais e não funcionais de acordo com os requisitos do cliente.

## Scripts Gerados

Para utilizar os scripts gerados, acesse a __[branch K6](https://gitlab.com/compass_anniely/final_project/-/tree/k6?ref_type=heads)__, onde encontrará documentação detalhada sobre como usar os scripts e outras funcionalidades relacionadas.

## Contribuintes

- Artur Dantas Rodrigues
- Raphael Luiz Marinho
- Henrique Cardoso Lana

Agradeço a todos os contribuintes por seu auxílio neste projeto de teste.

## Licença
[LICENSE](https://gitlab.com/compass_anniely/final_project/-/blob/main/LICENSE)
